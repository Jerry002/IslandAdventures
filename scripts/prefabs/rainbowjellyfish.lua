local assets=
{
  	Asset("ANIM", "anim/rainbowjellyfish.zip"),
  	Asset("ANIM", "anim/meat_rack_food.zip"),
	
	Asset("ANIM", "anim/scale_o_matic_rainbowjellyfish.zip"),
}

local prefabs =
{
    "rainbowjellyfish_light",
}

local function CalcNewSize()
	return math.random()
end

local function PlayDeadAnim(inst)
  	inst.AnimState:PlayAnimation("death_ground", true)
  	inst.AnimState:PushAnimation("idle_ground", true)
end

local function OnDropped(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
    local onwater = TheWorld.Map:IsPassableAtPoint(x, y, z)
    local replacement = SpawnPrefab(onwater and "rainbowjellyfish_dead" or "rainbowjellyfish_planted")
    replacement.Transform:SetPosition(x, y, z)
    inst:Remove()
    if onwater then
        replacement.AnimState:PlayAnimation("stunned_loop", true)
        replacement:DoTaskInTime(2.5, PlayDeadAnim)
    end
end

-----------------------------------------------------------------------

local function create_light(eater, lightprefab)
    if eater.wormlight ~= nil then
        if eater.wormlight.prefab == lightprefab then
            eater.wormlight.components.spell.lifetime = 0
            eater.wormlight.components.spell:ResumeSpell()
            return
        else
            eater.wormlight.components.spell:OnFinish()
        end
    end

    local light = SpawnPrefab(lightprefab)
    light.components.spell:SetTarget(eater)
    if light:IsValid() then
        if light.components.spell.target == nil then
            light:Remove()
        else
            light.components.spell:StartSpell()
        end
    end
end

local function item_oneaten(inst, eater)
    create_light(eater, "rainbowjellyfish_light")
end

local function commonfn()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.Transform:SetScale(0.8, 0.8, 0.8)

	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("rainbowjellyfish")
	inst.AnimState:SetBuild("rainbowjellyfish")

	inst:AddTag("jellyfish")

	inst.AnimState:SetRayTestOnBB(true)

	return inst
end

local function masterfn(inst)
    inst:AddComponent("inventoryitem")

    inst:AddComponent("inspectable")

    inst:AddComponent("tradable")
	inst.components.tradable.goldvalue = TUNING.GOLD_VALUES.MEAT
	inst.components.tradable.dubloonvalue = TUNING.DUBLOON_VALUES.SEAFOOD
end

local function defaultfn()

	local inst = commonfn()

    inst.AnimState:PlayAnimation("idle_ground", true)

    inst:AddTag("show_spoilage")
	inst:AddTag("cookable")
    inst:AddTag("small_livestock") -- "hungry" instead of "stale"
    inst:AddTag("fish")
    inst:AddTag("smalloceancreature")
	inst:AddTag("lightbattery")

	MakeInventoryFloatable(inst)
    inst.components.floater:UpdateAnimations("idle_water", "idle_ground")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

    masterfn(inst)

	inst:AddComponent("edible") --mermking and merm code is stupid
    inst.components.edible.foodtype = "NONE"

	inst:AddComponent("perishable")
	inst.components.perishable:SetPerishTime(TUNING.PERISH_ONE_DAY * 1.5)
	inst.components.perishable:StartPerishing()
	inst.components.perishable.onperishreplacement = "rainbowjellyfish_dead"

	inst.components.inventoryitem:SetOnDroppedFn(OnDropped)

	inst:ListenForEvent("on_landed", OnDropped)

	inst:AddComponent("cookable")
	inst.components.cookable.product = "rainbowjellyfish_cooked"

	inst:AddComponent("health")
	inst.components.health.murdersound = "ia/creatures/jellyfish/death_murder"

	inst:AddComponent("murderable")
	
	inst:AddComponent("lootdropper")
	inst.components.lootdropper:SetLoot({"rainbowjellyfish_dead"})
	
	MakeHauntableLaunchAndPerish(inst)
	
	inst:AddComponent("weighable")
	inst.components.weighable.type = TROPHYSCALE_TYPES.FISH
	inst.components.weighable:Initialize(TUNING.IA_WEIGHTS.RAINBOWJELLYFISH.min, TUNING.IA_WEIGHTS.RAINBOWJELLYFISH.max)
	inst.components.weighable:SetWeight(Lerp(TUNING.IA_WEIGHTS.RAINBOWJELLYFISH.min, TUNING.IA_WEIGHTS.RAINBOWJELLYFISH.max, CalcNewSize()))

	return inst

end

local function deadfn()
	local inst = commonfn()

	inst:AddTag("fishmeat")

	inst.Transform:SetScale(0.7, 0.7, 0.7)

	MakeInventoryFloatable(inst)
	inst.components.floater:UpdateAnimations("idle_water", "idle_ground")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("edible")
	inst.components.edible.foodtype = "MEAT"
	inst.components.edible:SetOnEatenFn(item_oneaten)
    inst.components.edible.secondaryfoodtype = FOODTYPE.MONSTER

	masterfn(inst)

	inst:AddComponent("perishable")
	inst.components.perishable:SetPerishTime(TUNING.PERISH_FAST)
	inst.components.perishable:StartPerishing()
	inst.components.perishable.onperishreplacement = "spoiled_food"

	inst.AnimState:PlayAnimation("idle_ground", true)

	inst:AddComponent("cookable")
	inst.components.cookable.product = "rainbowjellyfish_cooked"

	inst:AddComponent("dryable")
	inst.components.dryable:SetProduct("jellyjerky")
	inst.components.dryable:SetBuildFile("meat_rack_food_sw")
	inst.components.dryable:SetDryTime(TUNING.DRY_FAST)

	MakeHauntableLaunchAndPerish(inst)

	inst:AddComponent("stackable")
	inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM

	return inst
end


local function cookedfn()
	local inst = commonfn()

	inst:AddTag("fishmeat")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("edible")
	inst.components.edible.foodtype = "MEAT"
	inst.components.edible.foodstate = "COOKED"
	inst.components.edible.hungervalue = TUNING.CALORIES_MEDSMALL
	inst.components.edible.sanityvalue = 0
    inst.components.edible.secondaryfoodtype = FOODTYPE.MONSTER

	masterfn(inst)

	inst:AddComponent("perishable")
	inst.components.perishable:SetPerishTime(TUNING.PERISH_MED)
	inst.components.perishable:StartPerishing()
	inst.components.perishable.onperishreplacement = "spoiled_food"

	inst.components.inventoryitem.sinks = true --I checked sw they sink -Half

	MakeHauntableLaunchAndPerish(inst)

	inst.AnimState:PlayAnimation("cooked", true)
	inst:AddComponent("stackable")
	inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM
	return inst
end

-----------------------------------------------------------------------

local lightprefabs =
{
    "rainbowjellyfish_light_fx",
}

local function light_resume(inst, time)
    inst.fx:setprogress(1 - time / inst.components.spell.duration)
end

local function light_start(inst)
    inst.fx:setprogress(0)
end

local function pushbloom(inst, target)
    if target.components.bloomer ~= nil then
        target.components.bloomer:PushBloom(inst, "shaders/anim.ksh", -1)
    elseif not target:HasTag("trophyscale_fish") then
        target.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    end
end

local function popbloom(inst, target)
    if target.components.bloomer ~= nil then
        target.components.bloomer:PopBloom(inst)
    elseif not target:HasTag("trophyscale_fish") then
        target.AnimState:ClearBloomEffectHandle()
    end
end

local function OnOwnerChange(inst)
    local newowners = {}
    local owner = inst._target
    local isrider = false
    while true do
        newowners[owner] = true

        local rider = owner.components.rideable and owner.components.rideable:GetRider()
        local invowner = owner.components.inventoryitem and owner.components.inventoryitem.owner

        if inst._owners[owner] then
            inst._owners[owner] = nil
        else
            if owner.components.rideable then
                inst:ListenForEvent("riderchanged", inst._onownerchange, owner)
            end
            if not rider and owner.components.inventoryitem then
                inst:ListenForEvent("onputininventory", inst._onownerchange, owner)
                inst:ListenForEvent("ondropped", inst._onownerchange, owner)
            end
        end

        local nextowner = rider or invowner
        if not nextowner then break end
        isrider = rider ~= nil
        owner = nextowner
    end

    inst.fx.entity:SetParent(owner.entity)

    if inst._popbloom ~= nil and inst._popbloom ~= owner then
        popbloom(inst, inst._popbloom)
        if isrider then
            pushbloom(inst, owner)
            inst._popbloom = owner
        else
            inst._popbloom = nil
        end
    end

    for k, v in pairs(inst._owners) do
        if k:IsValid() then
            if k.components.inventoryitem then
                inst:RemoveEventCallback("onputininventory", inst._onownerchange, k)
                inst:RemoveEventCallback("ondropped", inst._onownerchange, k)
            end
            if k.components.rideable then
                inst:RemoveEventCallback("riderchanged", inst._riderchanged, k)
            end
        end
    end

    inst._owners = newowners
end

local function light_ontarget(inst, target)
    if target == nil or target:HasTag("playerghost") or target:HasTag("overcharge") then
        inst:Remove()
        return
    end

    local function forceremove()
        inst.components.spell:OnFinish()
    end

    inst._target = target
    target.wormlight = inst
    --FollowSymbol position still works on blank symbol, just
    --won't be visible, but we are an invisible proxy anyway.
    inst.Follower:FollowSymbol(target.GUID, "", 0, 0, 0)
    inst:ListenForEvent("onremove", forceremove, target)
    inst:ListenForEvent("death", function() inst.fx:setdead() end, target)

    if target:HasTag("player") then
        inst:ListenForEvent("ms_becameghost", forceremove, target)
        if target:HasTag("electricdamageimmune") then
            inst:ListenForEvent("ms_overcharge", forceremove, target)
        end
        inst.persists = false
    else
        inst.persists = not target:HasTag("critter")
    end

    pushbloom(inst, target)
    OnOwnerChange(inst)
end

local function light_onfinish(inst)
    local target = inst.components.spell.target
    if target ~= nil then
        target.wormlight = nil

        popbloom(inst, target)

        if target.components.rideable ~= nil then
            local rider = target.components.rideable:GetRider()
            if rider ~= nil then
                popbloom(inst, rider)
            end
        end
    end
end

local function light_onremove(inst)
    inst.fx:Remove()
end

local function lightfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddFollower()
    inst:Hide()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]

    inst:AddComponent("spell")
    inst.components.spell.spellname = "rainbowjellyfish"
    inst.components.spell.duration = TUNING.RAINBOWJELLYFISH_LIGHT_DURATION
    inst.components.spell.ontargetfn = light_ontarget
    inst.components.spell.onstartfn = light_start
    inst.components.spell.onfinishfn = light_onfinish
    inst.components.spell.resumefn = light_resume
    inst.components.spell.removeonfinish = true

    inst.persists = false --until we get a target
    inst.fx = SpawnPrefab("rainbowjellyfish_light_fx")
    inst.OnRemoveEntity = light_onremove

    inst._owners = {}
    inst._onownerchange = function() OnOwnerChange(inst) end

    return inst
end

-----------------------------------------------------------------------
local noupdate_parents = {
	["trophyscale_fish"] = true, --Hornet: I present, the hackiest hack of the land(Even when we pause the spell the fx still updates light frame, so i came up with this...)
}

local function OnUpdateLight(inst, dt)
    local frame =
        inst._lightdead:value() and
        math.ceil(inst._lightframe:value() * .9 + inst._lightmaxframe * .1) or
        (inst._lightframe:value() + dt)

    if frame >= inst._lightmaxframe then
        inst._lightframe:set_local(inst._lightmaxframe)
        inst._lighttask:Cancel()
        inst._lighttask = nil
    else
		local parent = inst.entity:GetParent()
		if parent and not noupdate_parents[parent.prefab] then
			inst._lightframe:set_local(frame)
		end
		
		--colours, only needs to happen if the light is still valid
		inst._colourframe = inst._colourframe + dt
		if inst._colourframe >= 120 then
			inst._colourframe = 0
			inst._colourprev = inst._colouridx
			inst._colouridx = inst._colouridx + 1
			if inst._colouridx > #inst._colours then
				inst._colouridx = 1
			end
		end
		
		--lerp to colour (lighttweener is not used in DST)
		local lerpk = inst._colourframe / 120
		inst.Light:SetColour(
			inst._colours[inst._colourprev][1] * (1 - lerpk) + inst._colours[inst._colouridx][1] * lerpk,
			inst._colours[inst._colourprev][2] * (1 - lerpk) + inst._colours[inst._colouridx][2] * lerpk,
			inst._colours[inst._colourprev][3] * (1 - lerpk) + inst._colours[inst._colouridx][3] * lerpk
		)
		
    end

    inst.Light:SetRadius(TUNING.RAINBOWJELLYFISH_LIGHT_RADIUS * (1 - inst._lightframe:value() / inst._lightmaxframe))
	
end

local function OnLightDirty(inst)
    if inst._lighttask == nil then
        inst._lighttask = inst:DoPeriodicTask(FRAMES, OnUpdateLight, nil, 1)
    end
    OnUpdateLight(inst, 0)
end

local function setprogress(inst, percent)
    inst._lightframe:set(math.max(0, math.min(inst._lightmaxframe, math.floor(percent * inst._lightmaxframe + .5))))
    OnLightDirty(inst)
end

local function setdead(inst)
    inst._lightdead:set(true)
    inst._lightframe:set(inst._lightframe:value())
end

local function lightfxfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddLight()
    inst.entity:AddNetwork()

    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.Light:SetRadius(0)
    inst.Light:SetIntensity(.8)
    inst.Light:SetFalloff(.5)
    inst.Light:SetColour(0, 0, 0)
    inst.Light:Enable(true)
    inst.Light:EnableClientModulation(true)

	inst._colours = {
		{0/255, 180/255, 255/255}, --skyblue
		{240/255, 230/255, 100/255}, -- ochre 
		{251/255, 30/255, 30/255}, -- red
	}
	inst._colouridx = 1
	inst._colourprev = #inst._colours
	inst._colourframe = 0

    inst._lightmaxframe = math.floor(TUNING.RAINBOWJELLYFISH_LIGHT_DURATION / FRAMES + .5)
    inst._lightframe = net_ushortint(inst.GUID, "rainbowjellyfish_light_fx._lightframe", "lightdirty")
    inst._lightframe:set(inst._lightmaxframe)
    inst._lightdead = net_bool(inst.GUID, "rainbowjellyfish_light_fx._lightdead")
    inst._lighttask = nil

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        inst:ListenForEvent("lightdirty", OnLightDirty)

        return inst
    end

    inst.setprogress = setprogress
    inst.setdead = setdead
    inst.persists = false

    return inst
end

return Prefab("rainbowjellyfish", defaultfn, assets),
Prefab("rainbowjellyfish_dead", deadfn, assets),
Prefab("rainbowjellyfish_cooked", cookedfn, assets),
Prefab("rainbowjellyfish_light", lightfn, nil, lightprefabs),
Prefab("rainbowjellyfish_light_fx", lightfxfn)
