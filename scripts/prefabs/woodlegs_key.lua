local assets_keys = {
    Asset("ANIM", "anim/woodlegs_key.zip"),
}

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    inst:Remove()
end

local function fn_keys(number, loot)
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBuild("woodlegs_key")
    inst.AnimState:SetBank("woodlegs_key")
    inst.AnimState:PlayAnimation("key"..number)

    MakeInventoryPhysics(inst)

    MakeInventoryFloatable(inst)
    inst.components.floater:UpdateAnimations("key"..number.."_water", "key"..number)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inventoryitem")

    inst:AddComponent("inspectable")

    inst:AddComponent("lootdropper")
    inst.components.lootdropper:AddChanceLoot(loot, 1)

    inst:AddComponent("klaussackkey")

    inst:AddComponent("workable")
	inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
	inst.components.workable:SetWorkLeft(1)
	inst.components.workable:SetOnFinishCallback(onhammered)

    MakeHauntableLaunch(inst)

    return inst
end

local function MakeKey(number, loot)
    return Prefab("woodlegs_key"..number, function()
        return fn_keys(number, loot)
    end, assets_keys, {})
end

return MakeKey(1, "boneshard"), MakeKey(2, "goldnugget"), MakeKey(3, "trinket_17")