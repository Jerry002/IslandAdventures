-- must use modimport. Otherwise, it collapse will when mod is tuen on again
modimport("scripts/map/startlocations/volcano")
modimport("scripts/map/tasksets/volcanoset")
modimport("scripts/map/levels/volcano")

-- shipwrecked
modimport("scripts/map/startlocations/shipwrecked")
modimport("scripts/map/tasksets/shipwreckedset")
modimport("scripts/map/levels/shipwrecked")

-- postinit
modimport("postinit/map/levels/shipwrecked")
