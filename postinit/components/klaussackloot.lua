local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

local giant_loot1 =
{
    "tigereye",
    "hivehat",
    "turbine_blades",
    "shroom_skin",
    "mandrake"
}

local giant_loot2 =
{
    "red_mushroomhat_blueprint",
    "green_mushroomhat_blueprint",
    "blue_mushroomhat_blueprint",
    "mushroom_light2_blueprint",
    "mushroom_light_blueprint",
    "bundlewrap_blueprint"
}

local giant_loot3 =
{
    "royal_jelly",
    "doydoyegg",
    "spiderhat",
    "shark_gills",
    "magic_seal",
	"malbatross_beak",
	"tallbirdegg",
    "quackenbeak"
}

IAENV.AddComponentPostInit("klaussackloot", function(self)
    self.inst:DoTaskInTime(0, function() -- a bad workaround for using rollklausloot only after it gets changed
        self:RollKlausLoot()
    end)
end)

local KlausSackLoot = require("components/klaussackloot")

local RollKlausLoot_old = KlausSackLoot.RollKlausLoot

function KlausSackLoot:RollKlausLoot(...)
    local result = RollKlausLoot_old and RollKlausLoot_old(self, ...)

    if TheWorld:HasTag("island") then
        local items = {}

        table.insert(items, giant_loot1[math.random(#giant_loot1)])

        if #self.loot[4] == 4 then
            table.insert(items, giant_loot2[math.random(#giant_loot2)])
        end
        
        local i1 = math.random(#giant_loot3)
        local i2 = math.random(#giant_loot3 - 1)

        table.insert(items, giant_loot3[i1])
        table.insert(items, giant_loot3[i2 == i1 and #giant_loot3 or i2])

        self.loot[4] = items
    end

    return result
end
