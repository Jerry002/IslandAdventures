local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

IAENV.AddPrefabPostInit("ipecacsyrup", function(inst)
    if not TheWorld.ismastersim then
        return
    end
    local oneaten_old = inst.components.edible.oneaten
    inst.components.edible.oneaten = function(inst, eater, ...)
        if eater.prefab == "wilbur" then
            eater:AddDebuff("ipecacsyrup_buff", "ipecacsyrup_buff")
        else
            return oneaten_old and oneaten_old(inst, eater, ...)
        end
    end
end)

IAENV.AddPrefabPostInit("ipecacsyrup_buff", function(inst)
    if not TheWorld.ismastersim then
        return
    end
    local timerdone1_old = inst.event_listeners.timerdone[inst][1]
    inst.event_listeners.timerdone[inst][1] = function(inst, data, ...)
        if inst.components.debuff.target.prefab == "wilbur" then
            if inst._tick_count <= 0 then
                inst.components.debuff:Stop()
            else
                inst._tick_count = inst._tick_count - 1
                inst.components.timer:StartTimer("pooptick", TUNING.IPECAC_TICK_TIME)
            end
        
            local target = inst.components.debuff.target
            if target then
                local poop = SpawnPrefab("poop")
        
                poop.Transform:SetPosition(target.Transform:GetWorldPosition())
                
                local periodicspawner = target.components.periodicspawner
                if periodicspawner ~= nil and periodicspawner.onspawn ~= nil then
                    periodicspawner.onspawn(target, poop)
                end

                target.sg:GoToState("hit")
                local leftover = target.components.hunger.current - TUNING.IPECAC_DAMAGE_PER_TICK
                target.components.hunger:DoDelta(-TUNING.IPECAC_DAMAGE_PER_TICK)
                target.components.health:DoDelta(math.min(0, leftover), nil, inst.prefab, nil, inst)
                target.SoundEmitter:PlaySound("meta2/wormwood/laxative_poot")
            end
        else
            return timerdone1_old(inst, data, ...)
        end
    end
end)