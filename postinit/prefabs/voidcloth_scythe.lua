local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local HARVEST_AFTERFINDINGONEOFTAGS = {"pickable", "HACK_workable"}
local HARVEST_CANTTAGS  = {"INLIMBO", "FX"}
local HARVEST_ONEOFTAGS = {"plant", "lichen", "oceanvine", "kelp"}

local function hacked(inst, data)
    for k,v in pairs(data.loot) do
        Launch(v, data.hacker, 1.5)
    end
    inst:RemoveEventCallback("hacked", hacked)
end

local DoScythe_old

local function DoScythe(inst, target, doer, ...)
    if target.components.pickable or target.components.hackable then
        local doer_pos = doer:GetPosition()
        local x, y, z = doer_pos:Get()

        local doer_rotation = doer.Transform:GetRotation()

        local ents = TheSim:FindEntities(x, y, z, TUNING.VOIDCLOTH_SCYTHE_HARVEST_RADIUS, nil, HARVEST_CANTTAGS, HARVEST_ONEOFTAGS)
        for _, ent in pairs(ents) do
            if ent:IsValid() and ent:HasOneOfTags(HARVEST_AFTERFINDINGONEOFTAGS) and inst:IsEntityInFront(ent, doer_rotation, doer_pos) then
                if ent.components.pickable ~= nil then
                    inst:HarvestPickable(ent, doer)
                elseif ent.components.hackable ~= nil then
                    local workamount = 3 * doer.components.workmultiplier:GetMultiplier(ACTIONS.HACK)
                    if ent.components.hackable.hacksleft <= workamount then
                        ent:ListenForEvent("hacked", hacked)
                    end
                    ent.components.hackable:Hack(doer, workamount, true)
                end
            end
        end
    end

    return DoScythe_old(inst, target, doer, ...)
end

IAENV.AddPrefabPostInit("voidcloth_scythe", function(inst)
    if not DoScythe_old then
        DoScythe_old = inst.DoScythe
    end
    inst.DoScythe = DoScythe
end)
